/**
 * Automatically generated file. DO NOT MODIFY
 */
package eu.andret.kalendarzswiatnietypowych;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String PACKAGE_NAME = "eu.andret.kalendarzswiatnietypowych";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 42;
  public static final String VERSION_NAME = "";
}
