/**
 * Author: Andret
 * Copying and modifying allowed only keeping git link
 */
package eu.andret.kalendarzswiatnietypowych.fragment;

import java.util.Random;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import eu.andret.kalendarzswiatnietypowych.R;
import eu.andret.kalendarzswiatnietypowych.utils.Data;
import eu.andret.kalendarzswiatnietypowych.utils.Data.Prefs;
import eu.andret.kalendarzswiatnietypowych.utils.HolidayCalendar;
import eu.andret.kalendarzswiatnietypowych.utils.HolidayCalendar.HolidayMonth.HolidayDay;

public class MonthDayFragment extends Fragment {
	private int day, month;
	private static final Random random = new Random();
	
	@Override
	public void setArguments(Bundle args) {
		super.setArguments(args);
		month = args.getInt("day");
		day = args.getInt("day");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_day, parent, false);
		HolidayDay holidays = HolidayCalendar.getInstance(getActivity()).getMonth(month).getDay(day);
		if (holidays == null) {
			view.findViewById(R.id.fragment_day_image_sad).setVisibility(View.VISIBLE);
			view.findViewById(R.id.fragment_day_text_empty).setVisibility(View.VISIBLE);
			view.findViewById(R.id.fragment_day_text_empty).setBackgroundColor(Color.GRAY);
		}
		SharedPreferences theme = Data.getPreferences(getActivity(), Data.Prefs.THEME);
		if (theme.getBoolean(getContext().getResources().getString(R.string.settings_theme_colorized), false)) {
			random.setSeed(holidays.getSeed());
			boolean dark = Data.getColors(Integer.parseInt(Data.getPreferences(getActivity(), Prefs.THEME).getString(getContext().getResources().getString(R.string.settings_theme_app), "1"))).dark;
			int c = Color.rgb(random.nextInt(127) + (dark ? 0 : 127), random.nextInt(127) + (dark ? 0 : 127), random.nextInt(127) + (dark ? 0 : 127));
			((RelativeLayout) view.findViewById(R.id.fragment_day_relative_main)).setBackgroundColor(c);
		}
		// ((ListView) view.findViewById(R.id.fragment_day_month_list_holidays)).setAdapter(new
		// HolidayAdapter(getActivity(), holidays, color, true));
		return view;
	}
}
