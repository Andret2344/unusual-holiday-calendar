/**
 * Author: Andret
 * Copying and modifying allowed only keeping git link
 */
package eu.andret.kalendarzswiatnietypowych.adapters;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import eu.andret.kalendarzswiatnietypowych.R;
import eu.andret.kalendarzswiatnietypowych.utils.Data;
import eu.andret.kalendarzswiatnietypowych.utils.HolidayCalendar.HolidayMonth.HolidayDay;
import eu.andret.kalendarzswiatnietypowych.utils.HolidayCalendar.HolidayMonth.HolidayDay.Holiday;

public class MonthHolidayAdapter extends ArrayAdapter<Holiday> {
	private final int color;
	
	public static class ViewHolder {
		public TextView holiday;
		public RelativeLayout background;
		public ImageView report;
	}
	
	public MonthHolidayAdapter(Context context, HolidayDay holiday, int color) {
		super(context, R.layout.adapter_holiday, holiday == null ? new ArrayList<Holiday>() : new ArrayList<Holiday>(holiday.getHolidays()));
		this.color = color;
	}
	
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.adapter_holiday, parent, false);
			holder = new ViewHolder();
			holder.holiday = (TextView) convertView.findViewById(R.id.adapter_holiday_text_holiday);
			holder.background = (RelativeLayout) convertView.findViewById(R.id.adapter_holiday_relative_main);
			holder.report = (ImageView) convertView.findViewById(R.id.adapter_holiday_image_report);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.holiday.setText(getItem(position) + ".");
		SharedPreferences theme = Data.getPreferences(getContext(), Data.Prefs.THEME);
		Data.AppColorSet color = Data.getColors(Integer.parseInt(theme.getString(getContext().getResources().getString(R.string.settings_theme_app), "1")));
		holder.holiday.setTextColor(color.forground);
		holder.background.setBackgroundColor(this.color);
		convertView.setBackgroundColor(this.color);
		
		holder.report.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final SharedPreferences tutorial = Data.getPreferences(getContext(), Data.Prefs.TUTORIAL);
				boolean reportInfo = tutorial.getBoolean(getContext().getResources().getString(R.string.settings_tutorial_reports), false);
				if (!reportInfo) {
					AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
					LayoutInflater adbInflater = LayoutInflater.from(getContext());
					View eulaLayout = adbInflater.inflate(R.layout.checkbox, parent, false);
					final CheckBox dontShowAgain = (CheckBox) eulaLayout.findViewById(R.id.skip);
					dontShowAgain.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							SharedPreferences.Editor editor = tutorial.edit();
							editor.putBoolean(getContext().getResources().getString(R.string.settings_tutorial_reports), dontShowAgain.isChecked());
							editor.apply();
						}
					});
					alert.setView(eulaLayout);
					alert.setTitle(R.string.caution);
					alert.setMessage(R.string.report_tutorial_info);
					alert.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							holder.report.setVisibility(View.INVISIBLE);
							getItem(position).report();// XXX
						}
					});
					
					alert.setNegativeButton(R.string.no, null);
					alert.show();
				} else {
					holder.report.setVisibility(View.INVISIBLE);
					getItem(position).report();// XXX
				}
			}
		});
		return convertView;
	}
}
