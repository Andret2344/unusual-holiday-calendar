/**
 * Author: Andret
 * Copying and modifying allowed only keeping git link
 */
package eu.andret.kalendarzswiatnietypowych.fragment;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import eu.andret.kalendarzswiatnietypowych.R;
import eu.andret.kalendarzswiatnietypowych.activities.MainActivity;
import eu.andret.kalendarzswiatnietypowych.adapters.DayAdapter;
import eu.andret.kalendarzswiatnietypowych.utils.Data;
import eu.andret.kalendarzswiatnietypowych.utils.HolidayCalendar;
import eu.andret.kalendarzswiatnietypowych.utils.HolidayCalendar.HolidayMonth.HolidayDay;

public class MonthFragment extends Fragment {
	private GridView grid;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		final View month = inflater.inflate(R.layout.fragment_month, parent, false);
		final int current = getArguments().getInt("month");
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, current);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.get(Calendar.DAY_OF_YEAR);
		if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			c.add(Calendar.DATE, -1);
		}
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.get(Calendar.DAY_OF_YEAR);
		Calendar before = (Calendar) c.clone();
		
		c.set(Calendar.MONTH, current);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.get(Calendar.DAY_OF_YEAR);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		int w = c.get(Calendar.WEEK_OF_YEAR);
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		c.set(Calendar.WEEK_OF_YEAR, w);
		c.get(Calendar.DAY_OF_YEAR);
		c.add(Calendar.DAY_OF_YEAR, 1);
		c.get(Calendar.DAY_OF_YEAR);
		Calendar after = (Calendar) c.clone();
		
		int diffDays = (int) TimeUnit.MILLISECONDS.toDays(after.getTimeInMillis() - before.getTimeInMillis());
		int diffWeeks = diffDays / 7 + (diffDays % 7 == 0 ? 0 : 1);
		if (diffWeeks < 6) {
			after.add(Calendar.WEEK_OF_MONTH, 6 - diffWeeks);
		}
		
		SharedPreferences theme = Data.getPreferences(getActivity(), Data.Prefs.THEME);
		Data.AppColorSet color = Data.getColors(Integer.parseInt(theme.getString(getContext().getResources().getString(R.string.settings_theme_app), "1")));
		final List<HolidayDay> holidays = HolidayCalendar.getInstance(getContext()).getHolidayDaysInDateRange(before, after, true);
		grid = (GridView) month.findViewById(R.id.fragment_month_grid_days);
		grid.measure(0, 0);
		final DayAdapter adapter = new DayAdapter(getActivity(), holidays, current);
		grid.setAdapter(adapter);
		grid.post(new Runnable() {
			@Override
			public void run() {
				int x = grid.getMeasuredHeight() / 6 - 1;
				for (int i = 0; i < grid.getChildCount(); i++) {
					View v = grid.getChildAt(i);
					ViewGroup.LayoutParams lp = v.getLayoutParams();
					lp.height = x;
					v.setLayoutParams(lp);
				}
				MainActivity.getInstance().dismissPreloader();
			}
		});
		month.findViewById(R.id.fragment_month_grid_days).setBackgroundColor(color.background);
		return month;
	}
}
