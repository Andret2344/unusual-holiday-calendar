/**
 * Author: Andret
 * Copying and modifying allowed only keeping git link
 */
package eu.andret.kalendarzswiatnietypowych.adapters;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import eu.andret.kalendarzswiatnietypowych.R;
import eu.andret.kalendarzswiatnietypowych.activities.DayActivity;
import eu.andret.kalendarzswiatnietypowych.utils.Data;
import eu.andret.kalendarzswiatnietypowych.utils.Data.MyColor;
import eu.andret.kalendarzswiatnietypowych.utils.Data.Prefs;
import eu.andret.kalendarzswiatnietypowych.utils.HolidayCalendar.HolidayMonth.HolidayDay;

public class DayAdapter extends ArrayAdapter<HolidayDay> {
	private static final Calendar calendar = Calendar.getInstance();
	private final static Random random = new Random();
	private final int month;
	
	private static class ViewHolder {
		private TextView date, holiday, more;
		private ImageView sad;
	}
	
	public DayAdapter(Context context, List<HolidayDay> holidays, int month) {
		super(context, R.layout.adapter_day, holidays);
		this.month = month;
	}
	
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.adapter_day, parent, false);
			holder = new ViewHolder();
			holder.date = (TextView) convertView.findViewById(R.id.adapter_day_text_number);
			holder.holiday = (TextView) convertView.findViewById(R.id.adapter_day_text_holiday);
			holder.more = (TextView) convertView.findViewById(R.id.adapter_day_text_more);
			holder.sad = (ImageView) convertView.findViewById(R.id.adapter_day_image_sad);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		SharedPreferences theme = Data.getPreferences(getContext(), Data.Prefs.THEME);
		Data.AppColorSet color = Data.getColors(Integer.parseInt(theme.getString(getContext().getResources().getString(R.string.settings_theme_app), "1")));
		
		holder.date.setTextColor(color.forground);
		holder.holiday.setTextColor(color.forground);
		holder.more.setTextColor(color.forground);
		convertView.setBackgroundColor(color.background);
		
		if (getItem(position).getDay() == calendar.get(Calendar.DAY_OF_MONTH) && getItem(position).getMonth().getMonth() == calendar.get(Calendar.MONTH) + 1) {
			convertView.setBackgroundColor(color.dark ? Color.rgb(55, 0, 0) : Color.rgb(200, 255, 255));
		}
		
		HolidayDay ho = getItem(position);
		if (ho.getMonth().getMonth() != month + 1) {
			convertView.setBackgroundColor(color.dark ? MyColor.GRAY_DARK : MyColor.GRAY_LIGHT);
		} else if (theme.getBoolean(getContext().getResources().getString(R.string.settings_theme_colorized), false)) {
			random.setSeed(getItem(position).getSeed());
			boolean dark = Data.getColors(Integer.parseInt(Data.getPreferences(getContext(), Prefs.THEME).getString(getContext().getResources().getString(R.string.settings_theme_app), "1"))).dark;
			int c = Color.rgb(random.nextInt(127) + (dark ? 0 : 127), random.nextInt(127) + (dark ? 0 : 127), random.nextInt(127) + (dark ? 0 : 127));
			convertView.setBackgroundColor(c);
		}
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getContext(), DayActivity.class);
				intent.putExtra("day", getItem(position).getDay());
				intent.putExtra("month", getItem(position).getMonth().getMonth() - 1);
				intent.putExtra("from", "calendar");
				getContext().startActivity(intent);
			}
		});
		
		boolean full = true;
		if (ho == null || ho.countHolidays(theme.getBoolean(getContext().getResources().getString(R.string.settings_usual_holidays), false)) == 0) {
			holder.sad.setVisibility(View.VISIBLE);
			holder.holiday.setText("");
		} else {
			String text = ho.getHolidaysList(theme.getBoolean(getContext().getResources().getString(R.string.settings_usual_holidays), false)).get(0).getText();
			holder.sad.setVisibility(View.INVISIBLE);
			String[] arr = text.split(" ");
			String result = "";
			final int words = 4;
			if (arr.length <= words) {
				result = text;
			} else {
				for (int i = 0; i < words; i++) {
					result += " " + arr[i];
				}
				result += "...";
				full = false;
			}
			if (ho.find(text).isUsual()) {
				holder.holiday.setTypeface(null, Typeface.BOLD);
			}
			holder.holiday.setText(result);
		}
		holder.date.setText(String.valueOf(getItem(position).getDay()));
		
		int number = getItem(position).countHolidays(theme.getBoolean(getContext().getResources().getString(R.string.settings_usual_holidays), false)) - (full ? 1 : 0);
		if (number > 0) {
			holder.more.setText(number + " " + getContext().getResources().getString(R.string.see_more));
		}
		return convertView;
	}
}
