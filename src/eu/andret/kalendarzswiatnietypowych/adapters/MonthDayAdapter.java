/**
 * Author: Andret
 * Copying and modifying allowed only keeping git link
 */
package eu.andret.kalendarzswiatnietypowych.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import eu.andret.kalendarzswiatnietypowych.fragment.MonthDayFragment;

public class MonthDayAdapter extends FragmentStatePagerAdapter {
	private final int month, day;
	
	public MonthDayAdapter(FragmentManager fm, int month, int day) {
		super(fm);
		this.month = month;
		this.day = day;
	}
	
	@Override
	public Fragment getItem(int id) {
		MonthDayFragment fragment = new MonthDayFragment();
		Bundle args = new Bundle();
		args.putInt("month", month);
		args.putInt("day", day);
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public int getCount() {
		return 12;
	}
}
